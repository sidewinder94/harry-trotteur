﻿/*
 *     _|    _|  _|_|_|_|_|  _|_|_|      _|_|_|
 *     _|    _|      _|      _|    _|  _|
 *     _|_|_|_|      _|      _|_|_|    _|
 *     _|    _|      _|      _|        _|
 *     _|    _|      _|      _|          _|_|_|
 */

#include <stdlib.h>
#include <stdio.h>
#include "game.h"
#include "play.h"

/**
 * HTPC_play:
 *
 * Fonction principale qui réalise un tour de jeu.
 * Le joueur qui réalise l'action est stocké dans la structure level.
 *
 * @action: l'action à effectuer
 * @level: le niveau dans lequel réaliser l'action
 *
 * Since: 0.1
**/
void
HTPC_play(HTPC_action *action,
          HTPC_level *level)
{
    switch(action->act)
    {

        case ACTION_MOVE_UP: // déplacements
        case ACTION_MOVE_DOWN:
        case ACTION_MOVE_RIGHT:
        case ACTION_MOVE_LEFT:
            HTPC_play_move(action, level);
            break;

        case ACTION_QUIT: // Quitter
            level->finish = HTPC_TRUE;
            break;

        case ACTION_CLOAK_ON:  // mettre la cape
        case ACTION_CLOAK_OFF: // déposer la cape
            HTPC_play_cloak(action, level);
            break;

        default:
            break;
    }
}

/**
 * HTPC_play_cloak:
 *
 * Déposer ou utiliser une cape.
 *
 * @action: l'action à effectuer (mettre ou retirer la cape)
 * @level: le niveau dans lequel réaliser l'action
 *
 * Since: 0.1
**/
void
HTPC_play_cloak(HTPC_action *action,
                HTPC_level *level)
{
    HTPC_player *player = NULL;

    if(action == NULL)
        return;

    if(level == NULL)
        return;

    player = level->player;

    switch(action->act)
    {
        case ACTION_CLOAK_ON:
            if(player->n_cloak > 0)
                player->is_visible = HTPC_FALSE;
            break;

        case ACTION_CLOAK_OFF:
            if(player->n_cloak > 0)
            {
                printf("salut !\n");
                player->is_visible = HTPC_TRUE;
                player->n_cloak--;
                HTPC_play_add_block(level, player->pos, BLOCK_CLOAK);
            }
            break;

        default:
            break;
    }
}

/**
 * HTPC_play_add_block:
 *
 * Ajoute un objet dans le niveau, par dessus un autre objet,
 * selon certaines contraintes.
 *
 * Chaque objet s'est vu attribuer un bit dans un octet :
 * BLOCK_GROUND  = 00000000   0x00
 * BLOCK_PANDA   = 00000001   0x01
 * BLOCK_PLAYER1 = 00000010   0x02
 * BLOCK_PLAYER2 = 00000100   0x04
 * BLOCK_CLOAK   = 00001000   0x08
 * BLOCK_POTION  = 00010000   0x10
 * BLOCK_WALL    = 00100000   0x20
 *
 * Pour fusionner deux objets, on utilise l'opérateur OU :
 * BLOCK_PLAYER1                = 00000010
 * BLOCK_POTION                 = 00010000
 * BLOCK_PLAYER1 | BLOCK_POTION = 00010010   0x12
 *
 * Puis pour récupérer une valeur précise :
 * block               = 00001001
 * BLOCK_PANDA         = 00000001
 * block & BLOCK_PANDA = 00000001   0x01
 *
 * Les combinaisons autorisées sont :
 * PANDA  + PLAYER1 = 00000011 0x03
 * POTION + PLAYER1 = 00010010 0x12
 * CLOAK  + PLAYER1 = 00001010 0x0A
 *
 * PANDA  + PLAYER2 = 00000101 0x05
 * POTION + PLAYER2 = 00010100 0x14
 * CLOAK  + PLAYER2 = 00001100 0x0C
 *
 * POTION + PANDA   = 00010001 0x11
 * CLOAK  + PANDA   = 00001001 0x09
 *
 * @level: le niveau à modifier
 * @pos: la position du bloc à modifier
 * @block: le bloc à ajouter
 *
 * Since: 0.1
**/
HTPC_BOOL
HTPC_play_add_block(HTPC_level *level,
                    HTPC_position pos,
                    HTPC_BLOCK block)
{
    unsigned char new_block = 0;

    if(level == NULL)
        return HTPC_FALSE;

    if(pos.x >= level->size_w
    || pos.y >= level->size_h)
        return HTPC_FALSE;

    new_block = level->blocks[pos.x][pos.y] | block;

    /* cas spécial : quand on passe GROUND en argument */
    /* on efface la case */
    if(block == BLOCK_GROUND)
        new_block = BLOCK_GROUND;

    switch(new_block)
    {
        case (BLOCK_PANDA  | BLOCK_PLAYER1):
        case (BLOCK_PANDA  | BLOCK_PLAYER2):
            level->finish = HTPC_TRUE;
        case BLOCK_PLAYER1:
        case BLOCK_PLAYER2:
        case BLOCK_PANDA:
        case BLOCK_POTION:
        case BLOCK_CLOAK:
        case (BLOCK_POTION | BLOCK_PLAYER1):
        case (BLOCK_CLOAK  | BLOCK_PLAYER1):
        case (BLOCK_POTION | BLOCK_PLAYER2):
        case (BLOCK_CLOAK  | BLOCK_PLAYER2):
        case (BLOCK_POTION | BLOCK_PANDA):
        case (BLOCK_CLOAK  | BLOCK_PANDA):
            level->blocks[pos.x][pos.y] = new_block;
            break;

        case BLOCK_GROUND: // si on passe le sol : efface la case
            level->blocks[pos.x][pos.y] = BLOCK_GROUND;
            break;

        default:
            return HTPC_FALSE;
            break;
    }

    return HTPC_TRUE;
}

HTPC_BOOL
HTPC_play_delete_block(HTPC_level *level,
                       HTPC_position pos,
                       HTPC_BLOCK block)
{
    unsigned char new_block = 0;

    if(level == NULL)
        return HTPC_FALSE;

    if(pos.x >= level->size_w
    || pos.y >= level->size_h)
        return HTPC_FALSE;

    /* tester l'existence du bloc à supprimer */
    if(level->blocks[pos.x][pos.y] & block)
        new_block = level->blocks[pos.x][pos.y] ^ block;
    else
        return HTPC_FALSE;

    /* appliquer */
    level->blocks[pos.x][pos.y] = new_block;
    return HTPC_TRUE;
}

/**
 * HTPC_play_collision:
 *
 * Since: 0.1
**/
HTPC_BOOL
HTPC_play_collision(HTPC_position dest,
                    HTPC_level *level,
                    HTPC_BOOL *potion)
{
    HTPC_BOOL collision = HTPC_FALSE;
    HTPC_player *player = NULL;
    unsigned char block = 0;

    if(level == NULL)
        return HTPC_TRUE;

    player = level->player;

    /* on vérifie que le block testé ne dépasse pas */
    if(dest.y >= level->size_w
    || dest.x >= level->size_h)
        return HTPC_TRUE;

    /* test du bloc */
    block = level->blocks[dest.x][dest.y];
    if(block == BLOCK_WALL)
    {
        if(player->n_potion > 0)
            *potion = HTPC_TRUE;

        collision = HTPC_TRUE;
    }

    if(((block & BLOCK_PLAYER1) && (player->id == BLOCK_PLAYER2))
    || ((block & BLOCK_PLAYER2) && (player->id == BLOCK_PLAYER1)))
        collision = HTPC_TRUE;

    /* test cape et potion */
    if(block & BLOCK_POTION)
    {
        player->n_potion++;
        HTPC_play_delete_block(level, dest, BLOCK_POTION);
    }

    if(block & BLOCK_CLOAK)
    {
        player->n_cloak++;
        HTPC_play_delete_block(level, dest, BLOCK_CLOAK);
    }

    return collision;
}

/**
 * HTPC_play_move:
 *
 * Since: 0.1
**/
void
HTPC_play_move(HTPC_action *action,
               HTPC_level *level)
{
    unsigned int n_potion = 0;
    unsigned int n_wall = 0;
    HTPC_BOOL potion = HTPC_FALSE;
    HTPC_position dest = {0, 0};
    HTPC_position temp = {0, 0};
    HTPC_position start = {0, 0};

    HTPC_player *player = NULL;

    if(action == NULL
    || level == NULL)
        return;

    player = level->player;
    dest.x = player->pos.x;
    dest.y = player->pos.y;

    temp.x = player->pos.x;
    temp.y = player->pos.y;

    start.x = player->pos.x;
    start.y = player->pos.y;

    /* calculer la destination */
    switch(action->act)
    {
        case ACTION_MOVE_UP:
            dest.x -= action->move;
            break;

        case ACTION_MOVE_DOWN:
            dest.x += action->move;
            break;

        case ACTION_MOVE_LEFT:
            dest.y -= action->move;
            break;

        case ACTION_MOVE_RIGHT:
            dest.y += action->move;
            break;

        default:
            break;
    }



    /* tant qu'on n'arrive pas à la position d'arrivée */
    while(player->pos.x != dest.x
       || player->pos.y != dest.y)
    {
        /* test de collision (sauf la position du perso) */
        if(temp.x != player->pos.x
        || temp.y != player->pos.y)
        {
            if(HTPC_play_collision(temp, level, &potion))
            {
                /* compter le nombre de mur à traverser */
                n_wall++;

                /* cas spécial de la potion */
                if(potion)
                {
                    /* si la collision actuelle (temp) est la destination voulue */
                    /* alors la destination choisie est un mur, donc on arrete */
                    if(temp.x == dest.x
                    && temp.y == dest.y)
                    {
                        /* on s'arrete */
                        dest.x = player->pos.x;
                        dest.y = player->pos.y;

                        n_potion++;

                        if(n_wall >= n_potion)
                            n_potion = 0;

                    }
                    else
                        n_potion++; // on utilise une potion

                    potion = HTPC_FALSE;
                }
                else // si on rencontre une collision et qu'on a pas de potion
                {
                    /* on s'arrete */
                    dest.x = player->pos.x;
                    dest.y = player->pos.y;
                }
            }
            else // pas de collision
            {
                /* on avance */
                player->pos.x = temp.x;
                player->pos.y = temp.y;

                n_wall = 0;
            }
        }

        /* direction du prochain déplacement */
        switch(action->act)
        {
            case ACTION_MOVE_UP:
                temp.x--;
                break;

            case ACTION_MOVE_DOWN:
                temp.x++;
                break;

            case ACTION_MOVE_LEFT:
                temp.y--;
                break;

            case ACTION_MOVE_RIGHT:
                temp.y++;
                break;

            default:
                break;
        }
    }

    /* application : déplacement du perso */
    HTPC_play_delete_block(level, start, player->id); // effacer l'ancienne position
    HTPC_play_add_block(level, player->pos, player->id);

    /* retrait d'une potion */
    if(n_potion > 0)
        player->n_potion--;
}

