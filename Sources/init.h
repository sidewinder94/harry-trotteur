#ifndef HTPC_H_DEF_INIT
#define HTPC_H_DEF_INIT
#include "game.h"

HTPC_BOOL init(HTPC_player *playerA,HTPC_player *playerB,HTPC_player *panda, 
	HTPC_level *level,HTPC_MODE *mode, const char *filename);

void perso(HTPC_player *player,
           HTPC_BLOCK id);
void mode_jeu(HTPC_MODE *mode);

HTPC_BOOL init_level(HTPC_level *level, FILE* level_file,HTPC_player *playerA,HTPC_player *playerB,HTPC_player *playerPanda,HTPC_MODE *mode);
HTPC_BOOL init_check(HTPC_level *level);
void clean_stdin(void);


#endif
