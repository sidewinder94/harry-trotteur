#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "init.h"
#include "limits.h"
#include "game.h"

/*
 * Initialise les données du jeu :
 *
 * Créé les joueurs et charge le niveau en mémoire.
 *
 * playerA: le premier joueur à initialiser
 * playerB: le deuxième joueur à initialiser
 * level: la structure niveau à initialiser
 * filename: le nom du fichier à ouvrir
 *
 * Retourne un booléen : Initialisation réussie ou pas.
*/

HTPC_BOOL
init(HTPC_player *playerA,
     HTPC_player *playerB,
	 HTPC_player *panda,
     HTPC_level *level,
     HTPC_MODE *mode,
     const char *filename)
{
	/*Déclaration de variable*/
	FILE* level_file = NULL;
	HTPC_BOOL error;
	
	/*Ouverture et test d'ouverture du fichier*/
	level_file = fopen(filename,"r");
	if (level_file != NULL)
	{
	    /*Appel des fonctions d'initialisation*/
        perso(playerA, BLOCK_PLAYER1);
        perso(playerB, BLOCK_PLAYER2);
		perso(panda, BLOCK_PANDA);
		
		/* mode de jeu */
        mode_jeu(mode);
        if(mode != NULL
        && *mode == MODE_RANDOM)
            srand((unsigned int)time(NULL));

        /* GO ! */
		error = init_level(level,level_file,playerA,playerB,panda,mode);
		if (error == HTPC_TRUE)
		{
			error = init_check(level);
		}
	    return error; 
	}
	else
	{
	    return HTPC_FALSE;
	}
}

/*
 * Initialise la structure d'un joueur.
 *
 * player: structure à initialiser
*/
void perso(HTPC_player *player,
           HTPC_BLOCK id)
{
    /* pour un joueur, demander son nom */
    if(id == BLOCK_PLAYER1
    || id == BLOCK_PLAYER2)
    {
        player->name = (char *)malloc(100*sizeof(char));
        if(id == BLOCK_PLAYER1)
            printf("Entrez le nom du premier joueur (A): ");
        else if(id == BLOCK_PLAYER2)
            printf("Entrez le nom du deuxieme joueur (B): ");
        scanf("%s",player->name);
    }
    else
        player->name = NULL;

    /* valeurs de base */
    player->id = id ;
    player->is_visible = HTPC_TRUE;
    player->n_cloak = 0;
    player->n_potion = 0;
    player->pos.x = 0;
    player->pos.y = 0;
}

/*
 * Demande à l'utilisateur le mode de jeu.
 *
 * mode: Le mode de jeu, cf. enum
*/
void mode_jeu(HTPC_MODE *mode)
{
	/*Definition d'une variable tampon pour l'entrée de l'utilisateur*/
    unsigned int entry = 0;
	/*Affichage du menu*/
    printf("Choisissez votre mode de jeu : \n"
    "1. Le panda reste immobile\n"
    "2. Le panda se deplace aleatoirement et ne peut utiliser de cape ou de potions\n"
    "3. Le panda se voit dote d'une intelligence et se deplace logiquement avec"
       " les meme regles que les joueurs\n\n");

	/*Boucle et affiche le message d'erreur tant que l'utilisateur
	 *n'entre pas une valeur correcte*/

	 scanf("%d", &entry);
	 while((entry < 1)||(entry > 3))
	 {
	     clean_stdin();
	     printf("Entree Incorrecte\n");
	     scanf("%d", &entry);
	 }
	 *mode = entry -1; // les modes commencent à 1
	

}

/*
 * Enregistre le niveau en mémoire.
 *
 * level: la structure à remplir
 * filename: le chemin vers le fichier
 *
 * Retourne un booléen : ouverture du fichier réussie ou pas.
*/
HTPC_BOOL
init_level(HTPC_level *level,
           FILE* level_file,
           HTPC_player *playerA,
           HTPC_player *playerB,
           HTPC_player *playerPanda,
           HTPC_MODE *mode)
{
	/*Déclaration des variables*/
    unsigned int i = 0;
    unsigned int j = 0;
    char c = 0;
    char line[LINE_SIZE];

	/*Initialisation des valeurs*/
    level->size_w = 0;
    level->size_h = 0;
	level->finish = HTPC_FALSE;
	level->player = NULL;
	level->mode = *mode;

    if (level_file != NULL)
    {
        /* Ce Bloc permet d'obtenir la taille du tableau */
        while (fgets(line,LINE_SIZE,level_file) != NULL)
        {
            if ((strlen(line))>level->size_w)
            {
                level->size_w = (unsigned int)strlen(line);
            }
			level->size_h++;
        }
        rewind(level_file);
        
        /* allocation du double tableau */
        level->blocks = (unsigned char **)malloc(sizeof(char*) * level->size_h);
        for (i = 0 ; i < level->size_h ; i++)
        {
            level->blocks[i] = (unsigned char *)malloc(level->size_w * sizeof(char));
        }
        
        /* remplissage du tableau */
        for (i = 0 ; i < level->size_h ; i++)
        {
            for (j = 0 ; j < level->size_w ; j++)
            {
                c = fgetc(level_file);
				/*Différenciation des caractères pour le stockage des types en mémoire*/
                switch(c)
                {
                    case 'M':
                        level->blocks[i][j] = BLOCK_WALL;
                        break;

                    case 'A':
                        level->blocks[i][j] = BLOCK_PLAYER1;
						playerA->pos.x = i;
						playerA->pos.y= j;
                        break;

                    case 'B':
                        level->blocks[i][j] = BLOCK_PLAYER2;
						playerB->pos.x = i;
						playerB->pos.y = j;
                        break;

                    case 'P':
                        level->blocks[i][j] = BLOCK_PANDA;
                        playerPanda->pos.x = i;
						playerPanda->pos.y= j;
                        break;

                    case 'C':
                        level->blocks[i][j] = BLOCK_CLOAK;
                        break;

                    case 'F':
                        level->blocks[i][j] = BLOCK_POTION;
                        break;

                    case ' ':
                        level->blocks[i][j] = BLOCK_GROUND;
                        break;

					case '*' :
						level->blocks[i][j] = BLOCK_BORDERLAND;
						break;

                    default:
                        level->blocks[i][j] = BLOCK_WALL;
                        break;
                }
            }
        }
        fclose(level_file);
        return HTPC_TRUE;
    }

    return HTPC_FALSE;
}

/* Fonction permettant de vérifier qu'un nombre minimum d'éléments sont présents
 *sur la carte
 *
 *level : Niveau a parcourir pour vérification
 *
**/
HTPC_BOOL init_check(HTPC_level *level)
{			
	/*Déclaration des variables*/
	unsigned int i,j;
	int tmp = 0, playerA = 0, playerB = 0, cloak = 0, panda = 0;

	/*Exploration du tableau*/
	for ( i = 0 ; i < level->size_h ; i++)
	{
		for ( j = 0 ; j < level->size_w ; j++)
		{
			tmp = level->blocks[i][j];
			switch(tmp)
			{
			case BLOCK_CLOAK:
				cloak++;
				break;
			case BLOCK_PANDA:
				panda++;
				break;
			case BLOCK_PLAYER1:
				playerA++;
				break;
			case BLOCK_PLAYER2:
				playerB++;
				break;
			}
		}
	}
	if ( (playerA == 1) && (playerB == 1) && (panda >= 1) && (cloak == 2))
	{
	    return HTPC_TRUE;
	}
	else
	{
	    printf("Map invalide.\n");
	    printf("Il faut minimum : 0 potions, 1 panda.\n");
	    printf("Il faut obligatoirement : 2 joueurs (A et B) et 2 capes.\n");
	    return HTPC_FALSE;
	}

}

void clean_stdin(void)
{
    int c;
    
    do {
        c = getchar();
    } while (c != '\n' && c != EOF);
}

