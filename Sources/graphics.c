#include <stdlib.h>
#include <stdio.h>
#ifdef _WIN32 //On a pas tous les headers situ�s au m�me endroit !!
#include <SDL.h>
#else
#include <SDL/SDL.h>
#endif

#include "graphics.h"
#include "game.h"
#include "limits.h"

static SDL_Surface *HTPC_GRAPHICS_SURFACE = NULL;
static SDL_Surface **HTPC_GRAPHICS_TILESET = NULL;

HTPC_BOOL
HTPC_graphics_init(unsigned int size_w,
                   unsigned int size_h,
                   const char *tileset)
{
    unsigned int i = 0;

    SDL_Rect rect = {0, 0, HTPC_TILE_SIZE, HTPC_TILE_SIZE};
    SDL_Surface *temp = NULL;

    if(SDL_Init(SDL_INIT_VIDEO) == -1)
        return HTPC_FALSE;

    HTPC_GRAPHICS_SURFACE = SDL_SetVideoMode(size_w * HTPC_TILE_SIZE, size_h * HTPC_TILE_SIZE,
                                             24, SDL_HWSURFACE | SDL_DOUBLEBUF);
    if(HTPC_GRAPHICS_SURFACE == NULL)
        return HTPC_FALSE;

    HTPC_GRAPHICS_TILESET = (SDL_Surface **)malloc(sizeof(SDL_Surface*) * HTPC_N_TILE);

    /* charger le tileset */
    temp = SDL_LoadBMP(tileset);

    for(i = 0; i < HTPC_N_TILE; i++)
    {
        HTPC_GRAPHICS_TILESET[i] = SDL_CreateRGBSurface(SDL_HWSURFACE, HTPC_TILE_SIZE, HTPC_TILE_SIZE, 24, 0, 0, 0, 0);
        if(HTPC_GRAPHICS_TILESET[i] != NULL)
        {
            SDL_FillRect(HTPC_GRAPHICS_TILESET[i], NULL, SDL_MapRGB(HTPC_GRAPHICS_TILESET[i]->format,
                                                                    0, 255, 0));
            SDL_BlitSurface(temp, &rect, HTPC_GRAPHICS_TILESET[i], NULL);
            SDL_SetColorKey(HTPC_GRAPHICS_TILESET[i], SDL_SRCCOLORKEY,
                            SDL_MapRGB(HTPC_GRAPHICS_TILESET[i]->format,
                                       0, 255, 0));
        }
        rect.x += HTPC_TILE_SIZE;
    }

    return HTPC_TRUE;
}

void
HTPC_graphics_display(HTPC_level *level,
                      HTPC_player *playerA,
                      HTPC_player *playerB,
                      HTPC_player *playerPanda)
{
    unsigned int i = 0;
    unsigned int j = 0;

    SDL_Rect blitter = {0, 0, HTPC_TILE_SIZE, HTPC_TILE_SIZE};

    for(i = 0; i < level->size_w; i++)
    {
        for(j = 0; j < level->size_h; j++)
        {
            SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_GROUND], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
            switch(level->blocks[j][i])
            {
                case BLOCK_WALL:
                    SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_WALL], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                case (BLOCK_POTION | BLOCK_PLAYER1):
                case (BLOCK_CLOAK  | BLOCK_PLAYER1):
                case BLOCK_PLAYER1:
                    if(playerA->is_visible) // si le joueur 1 est visible
                        SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_PLAYER1], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                case (BLOCK_POTION | BLOCK_PLAYER2):
                case (BLOCK_CLOAK  | BLOCK_PLAYER2):
                case BLOCK_PLAYER2:
                    if(playerB->is_visible) // si le joueur 2 est visible
                        SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_PLAYER2], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                case BLOCK_CLOAK:
                    SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_CLOAK], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                case BLOCK_POTION:
                    SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_POTION], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                case BLOCK_PANDA:
                case (BLOCK_PANDA  | BLOCK_PLAYER2):
                case (BLOCK_PANDA  | BLOCK_PLAYER1):
                case (BLOCK_POTION | BLOCK_PANDA):
                case (BLOCK_CLOAK  | BLOCK_PANDA):
                    if(playerPanda->is_visible) // si le panda est visible
                        SDL_BlitSurface(HTPC_GRAPHICS_TILESET[TILE_PANDA], NULL, HTPC_GRAPHICS_SURFACE, &blitter);
                    break;

                default:
                    break;
            }
            blitter.y += HTPC_TILE_SIZE;
        }
        blitter.x += HTPC_TILE_SIZE;
        blitter.y = 0;
    }

    SDL_Flip(HTPC_GRAPHICS_SURFACE);
    (void)level;
}

void
HTPC_graphics_free(void)
{
    unsigned int i = 0;

    for(i = 0; i < HTPC_N_TILE; i++)
        SDL_FreeSurface(HTPC_GRAPHICS_TILESET[i]);

    free(HTPC_GRAPHICS_TILESET);
    SDL_FreeSurface(HTPC_GRAPHICS_SURFACE);

    SDL_Quit();
}


