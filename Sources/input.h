#ifndef HTPC_H_DEF_INPUT
#define HTPC_H_DEF_INPUT

void input(HTPC_action *action, HTPC_player *player, HTPC_MODE mode);
void saisie(char *command);
HTPC_BOOL check(HTPC_action *action, char *command);

void
input_panda(HTPC_action *action,
            HTPC_player *panda,
            HTPC_MODE mode);
#endif
