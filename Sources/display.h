#ifndef HTPC_H_DEF_DISPLAY
#define HTPC_H_DEF_DISPLAY

/*
 * Fonction traitant les data du plateau de jeu
 * 
 * action : action à réaliser
 * level : niveau dans lequel réaliser le déplacement
 * playerA : joueur A
 * playerB : joueur B
 */
void display(HTPC_level *level,HTPC_player *playerA, HTPC_player *playerB);
/*
 * Fonction traitant les data du plateau de jeu
 * 
 * action : action à réaliser
 * level : niveau dans lequel réaliser le déplacement
 * code : code d'identification du symbole à afficher
 * playerA : joueur A
 * playerB : joueur B
 */
void gamechar(HTPC_level *level, unsigned int code,HTPC_player *playerA, HTPC_player *playerB);
#endif
