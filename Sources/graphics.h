#ifndef HTPC_H_DEF_GRAPHICS
#define HTPC_H_DEF_GRAPHICS

#include "game.h"

#define HTPC_TILE_SIZE 32
#define HTPC_N_TILE 9

typedef enum
{
    TILE_WALL,
    TILE_GROUND,
    TILE_STAIRS_DOWN,
    TILE_STAIRS_UP,
    TILE_CLOAK,
    TILE_POTION,
    TILE_PLAYER1,
    TILE_PLAYER2,
    TILE_PANDA
} HTPC_TILES;

HTPC_BOOL
HTPC_graphics_init(unsigned int size_w,
                   unsigned int size_h,
                   const char *tileset);

void
HTPC_graphics_display(HTPC_level *level,
                      HTPC_player *playerA,
                      HTPC_player *playerB,
                      HTPC_player *playerPanda);

void
HTPC_graphics_free(void);

#endif


