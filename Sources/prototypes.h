#ifndef HTPC_H_DEF_PROTOTYPES
#define HTPC_H_DEF_PROTOTYPES

/*
 * Initialise les données du jeu :
 *
 * Créé les joueurs et charge le niveau en mémoire.
 *
 * playerA: le premier joueur à initialiser
 * playerB: le deuxième joueur à initialiser
 * level: la structure niveau à initialiser
 * filename: le nom du fichier à ouvrir
 *
 * Retourne un booléen : Initialisation réussie ou pas.
*/
HTPC_BOOL
init(HTPC_player *playerA,
     HTPC_player *playerB,
     HTPC_level *level,
     HTPC_action *action,
     const char *filename);

/*
 * Initialise la structure d'un joueur.
 *
 * player: structure à initialiser
*/
void
perso(HTPC_player *player);

/*
 * Demande à l'utilisateur le mode de jeu.
 *
 * mode: Le mode de jeu, cf. enum
*/
void
mode(HTPC_MODE *mode);

/*
 * Enregistre le niveau en mémoire.
 *
 * level: la structure à remplir
 * filename: le chemin vers le fichier
 *
 * Retourne un booléen : ouverture du fichier réussie ou pas.
*/
HTPC_BOOL
level(HTPC_level *level,
      const char *filename);

/*
 * Récupère un saisie de l'utilisateur
 *
 * action: la structure à remplir
 * player: le joueur qui joue actuellement
*/
void
input(HTPC_action *action,
      HTPC_player *player);

/*
 * Permet à l'utilisateur de saisir les commandes
 *
 * command: chaine de caractères qui recevra la commande entrée
*/
void 
saisie(char *command);


/*
 * Vérifie la validité des commandes
 * Interprétation des commandes
 *
 * action: stockage de l'action décrite par la commande
 * command: commande entrée par l'utilisateur précédemment a v�rifier
*/
HTPC_BOOL check(HTPC_action *action, char *command);

/*
 * Fonction gérant le déplacement des joueurs.
 * Vérifie la collision
 *
 * action : action à réaliser
 * level : niveau dans lequel réaliser le déplacement
 * player : joueur qui réalise l'action
*/
void
move(HTPC_action *action,
     HTPC_level *level);
/*
 * Vérifie la présence de murs et d'objets
 * Récupere les objets pendant la vérification et les utilise si besoin
 *
 * dest : destination demandée par le joueur
 * player : joueur qui réalise l'action
 * level : niveau courant
*/
HTPC_BOOL
collision(HTPC_position dest,
          HTPC_player *player,
          HTPC_level *level);
/*
 * Permet de prendre et de déposer la cape
 * La cape est déposée sur la case ou se situe le joueur
 *
 * action : action liée à la cape mettre/déposer
 * player : joueur qui réalise l'action
 * level : niveau courant
*/
void
cloak(HTPC_action *action,
      HTPC_level *level);
/*
 * Réécrit la position des joueurs dans le niveau
 *
 * player : joueur courant
 * level : niveau courant
*/
void
write_level(HTPC_level *level);
/*
 * Efface le buffer
 * Affiche le niveau sur l'écran
 *
 * level : niveau courant
*/
void
display(HTPC_level *level);

#endif
