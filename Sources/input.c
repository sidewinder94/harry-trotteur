#include <stdlib.h>
#include <stdio.h>

#include "game.h"
#include "limits.h"
#include "input.h"

void saisie(char *command)
{
    printf("\nCommande : ");
    scanf("%s", command);
}

HTPC_BOOL check(HTPC_action *action, char *command)
{
    HTPC_BOOL verif = HTPC_FALSE;
    int number = 0;

    if(command == NULL
    || action == NULL)
        return HTPC_FALSE;

    /* remettre l'action à zéro */
    action->act = ACTION_UNKNOWN;
    action->move = 0;

    switch(command[0])
    {
        case 'G':
        case 'g':
            action->act = ACTION_MOVE_LEFT;
            break;

        case 'D':
        case 'd':
            action->act = ACTION_MOVE_RIGHT;
            break;

        case 'H':
        case 'h':
            action->act = ACTION_MOVE_UP;
            break;

        case 'B':
        case 'b':
            action->act = ACTION_MOVE_DOWN;
            break;

        case 'Q':
        case 'q':
            action->act = ACTION_QUIT;
            verif = HTPC_TRUE;
            break;

        case 'C':
        case 'c':
            action->act = ACTION_CLOAK_ON;
            verif = HTPC_TRUE;
            break;

        case 'P':
        case 'p':
            action->act = ACTION_CLOAK_OFF;
            verif = HTPC_TRUE;
            break;

        default:
            verif = HTPC_FALSE;
            break;
    }

    /* tester la valeur donnée */
    if(action->act == ACTION_MOVE_LEFT
    || action->act == ACTION_MOVE_RIGHT
    || action->act == ACTION_MOVE_UP
    || action->act == ACTION_MOVE_DOWN)
    {
        sscanf(&command[1], "%d", &number);
        if(number > 0 || number <= MOVE_MAX)
        {
            action->move = number;
            verif = HTPC_TRUE;
        }
        else
            verif = HTPC_FALSE;
    }

    return verif;
}

void input(HTPC_action *action, HTPC_player *player, HTPC_MODE mode)
{
    char *command = NULL;
    HTPC_BOOL error = HTPC_FALSE;

    if(player == NULL
    || action == NULL)
        return;

    command = (char *)malloc(sizeof(char) * COMMAND_SIZE);
    if(command == NULL)
        return;

    if(player->id == BLOCK_PANDA) // si le joueur est le panda
    {
        if(mode != MODE_NOMOVE)
            input_panda(action, player, mode);
        return;
    }

    printf("C'est au tour du joueur \"%s\".\n", player->name);
    printf("Potions : %d, Capes : %d\n", player->n_potion, player->n_cloak);

    do{
        saisie(command);
        error = check(action, command);
        if(error == HTPC_FALSE){
	        printf("Erreur de saisie.\n");
        }
    } while(error == HTPC_FALSE);

    free (command);
}

/**
 * input_panda:
 *
 * @action: L'action à modifier
 * @panda: Le statut du panda
 *
 * Fonction d'IA du panda.
 *
 * Since: 0.1
**/
void
input_panda(HTPC_action *action,
            HTPC_player *panda,
            HTPC_MODE mode)
{
    unsigned int min = 0;
    unsigned int max = 0;

    if(action == NULL
    || panda == NULL)
        return;

    if(mode == MODE_RANDOM)
    {
        /* choix de la direction */
        min = 0;
        max = 3;
        switch(rand() % (max + 1 - min) + min)
        {
            case 0:
                action->act = ACTION_MOVE_LEFT;
                break;

            case 1:
                action->act = ACTION_MOVE_RIGHT;
                break;

            case 2:
                action->act = ACTION_MOVE_UP;
                break;

            case 3:
                action->act = ACTION_MOVE_DOWN;
                break;

            default:
                break;
        }
        
        /* choix de la longueur */
        min = 0;
        max = MOVE_MAX;
        action->move = rand() % (max + 1 - min) + min;
    }
    else if(mode == MODE_IA)
    {
        /* IA */
    }
    
    (void)panda;
}


