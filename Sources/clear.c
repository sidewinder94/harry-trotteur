#include <stdlib.h>
#include <stdio.h>
#include "clear.h"

/* Fonction permettant d'éffacer l'écran de la console
 *
 * Utilise la fonction system pour envoyer la commande d'effacement
 * En fonciton de l'OS
 *
**/
void clear(){
    
#if defined _WIN32 /* _Win32 est généralement définit par les compilateurs dont la destination est
un systeme windows 32 ou 64 bits*/
    system("cls");
#else
   system("clear");
#endif
    
    
}
