#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <dirent.h>
#include <string.h>

const char*
menu(const char *dirname)
{
    DIR* rep = NULL;
	struct dirent* readfile = NULL;
	unsigned int i = 0;
	unsigned int choice = 0;
	char **list = NULL;

	rep = opendir(dirname);
	if (rep == NULL)
	{
	    printf("Need a .htpcmap file as parameter\n");
        printf("Usage : htpc file\n");
	    exit(EXIT_FAILURE);
	}

    /* lire, stocker et afficher */
	while((readfile = readdir(rep)) != NULL)
	{

	   printf("%d. %s\nFaites un choix : \n", i, readfile->d_name);
	   list = realloc(list, sizeof(char*) * i);
	   list[i] = malloc(sizeof(char) * strlen(readfile->d_name));
	   i++;
	}

    /* demander le numéro */
	scanf("%d", choice);
	clean_stdin();

	closedir(rep);
	return list[choice];
}
