/*                                         
 *     _|    _|  _|_|_|_|_|  _|_|_|      _|_|_|  
 *     _|    _|      _|      _|    _|  _|        
 *     _|_|_|_|      _|      _|_|_|    _|        
 *     _|    _|      _|      _|        _|        
 *     _|    _|      _|      _|          _|_|_|  
 */

#ifndef HTPC_H_DEF_GAME
#define HTPC_H_DEF_GAME

typedef enum
{
    ACTION_UNKNOWN,
    ACTION_QUIT,
    ACTION_MOVE_LEFT,
    ACTION_MOVE_RIGHT,
    ACTION_MOVE_UP,
    ACTION_MOVE_DOWN,
    ACTION_CLOAK_ON,
    ACTION_CLOAK_OFF
} HTPC_ACTION;

typedef enum
{
    MODE_NOMOVE = 0,
    MODE_RANDOM = 1,
    MODE_IA = 2,

    MODE_LAST
} HTPC_MODE;

typedef enum
{
    BLOCK_GROUND =      0x00,
    BLOCK_PANDA =       0x01,
    BLOCK_PLAYER1 =     0x02,
    BLOCK_PLAYER2 =     0x04,
    BLOCK_CLOAK =       0x08,
    BLOCK_POTION =      0x10,
    BLOCK_WALL =        0x20,
    BLOCK_BORDERLAND =  0xF0,
    BLOCK_UNKNOWN =     0xFF
} HTPC_BLOCK;

typedef enum
{
    HTPC_FALSE = 0,
    HTPC_TRUE = 1
} HTPC_BOOL;

typedef struct
{
    unsigned char x;
    unsigned char y;
} HTPC_position;

typedef struct
{
    HTPC_BLOCK id;
    char *name;
    unsigned char n_cloak;
    unsigned char n_potion;
    HTPC_BOOL is_visible;
    HTPC_position pos;
} HTPC_player;

typedef struct
{
	HTPC_ACTION act;
	unsigned char move;
} HTPC_action;

typedef struct
{
    unsigned int size_w;
    unsigned int size_h;
    unsigned char** blocks;
    HTPC_BOOL finish;
    HTPC_player *player;
    HTPC_MODE mode;
} HTPC_level;

#endif
