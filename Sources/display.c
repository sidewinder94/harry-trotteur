#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "game.h"
#include "limits.h"
#include "display.h"
#include "clear.h"
#if defined _WIN32
#include <windows.h>
#endif


/**
 * gamechar:
 *
 * Afficher un caractère lié à un code symbole
 *
 * @code: identificateur de symbole
 * @level: le niveau dans lequel réaliser l'action
 * @playerA: le joueur A
 * @playerB: le joueur B
 *
 * Since: 0.1
 **/
void gamechar(HTPC_level *level, unsigned int code,HTPC_player *playerA, HTPC_player *playerB){
    //Si WIN: récupère l'environnement de console
#if defined _WIN32
    HANDLE  hConsole;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#endif
    //SetConsoleTextAttribute: définit la couleur du texte dans le shell sous windows
    //\033[x;x: définit la couleur dans le shell sous linux
    switch (code) {
        case BLOCK_WALL:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 17);
            printf("\xDB");
#else
            printf("\033[34;40m");
            printf("█");
#endif
            break;
        case BLOCK_GROUND:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 0);
            printf(" ");
#else
            printf("\033[30;40m");
            printf("█");
#endif
            break;

        case (BLOCK_POTION | BLOCK_PLAYER1):
		case (BLOCK_CLOAK  | BLOCK_PLAYER1):
        case BLOCK_PLAYER1:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 12);
#else
            printf("\033[31;40m");
#endif
            if(playerA->is_visible) // si le joueur 1 est invisible
            {
                printf("A");
            }
            else
                printf(" ");
            break;
            
		case (BLOCK_CLOAK  | BLOCK_PLAYER2):
		case (BLOCK_POTION | BLOCK_PLAYER2):    
        case BLOCK_PLAYER2:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 2);
#else
            printf("\033[33;40m");
#endif
            if(playerB->is_visible) // si le joueur 1 est invisible
            {
                printf("B");
            }
            else
                printf(" ");
            break;
            
        case (BLOCK_POTION | BLOCK_PANDA):    
        case (BLOCK_PANDA  | BLOCK_PLAYER1):
		case (BLOCK_PANDA  | BLOCK_PLAYER2):
		case (BLOCK_CLOAK  | BLOCK_PANDA):
        case BLOCK_PANDA:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 192);
#else
            printf("\033[30;43m");
#endif
            printf("P");
            break;
            
        case BLOCK_CLOAK:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 1);
#else
            printf("\033[34;40m");
#endif
            printf("C");
            break;
            
        case BLOCK_POTION:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 160);
#else
            printf("\033[30;42m");
#endif
            printf("F");
            break;

        case BLOCK_BORDERLAND:
#if defined _WIN32
            SetConsoleTextAttribute(hConsole, 136);
            printf("\xDB");
#else
            printf("\033[37;47m");
            printf("█");
#endif
            break;
            
        case BLOCK_UNKNOWN:
//rien
            break;      
        default:
//rien
            break;
    }
}

/**
 * display:
 *
 * Afficher le terrain de jeu
 *
 * @level: le niveau dans lequel réaliser l'action
 * @playerA: le joueur A
 * @playerB: le joueur B
 *
 * Since: 0.1
 **/
void display(HTPC_level *level,HTPC_player *playerA, HTPC_player *playerB){
    unsigned int i=0,j=0;
    
//Si WIN: récupère l'environnement de console
#if defined _WIN32
    HANDLE  hConsole;
    hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
#endif
    //Effacement de la console
    clear();
    
    //Bordure supérieure
    for (j=0; j < level->size_w+2; j++) {
#if defined _WIN32
        SetConsoleTextAttribute(hConsole, 136);
        printf("\xDB");
#else
        printf("\033[37;47m");
        printf("█");
#endif
    }
    printf("\n");
    
    
    //Traitement du tableau de donnée de level
    for (i = 0 ; i < level->size_h ; i++)
    {
        gamechar(level, BLOCK_BORDERLAND,playerA,playerB);
        for (j = 0 ; j < level->size_w ; j++)
        {
            gamechar(level, level->blocks[i][j],playerA,playerB);
            
        }
        gamechar(level,BLOCK_BORDERLAND,playerA,playerB);
        printf ("\n");
    }
    
    //Bordure basse
    for (j=0; j < level->size_w+2; j++) {
#if defined _WIN32
        SetConsoleTextAttribute(hConsole, 136);
        printf("\xDB");
#else
        printf("\033[37;47m");
        printf("█");
#endif
    }

    //Rétablissement du style par défaut
#if defined _WIN32
    SetConsoleTextAttribute(hConsole, 7);
#else
    printf("\033[0m");
#endif
    printf("\n\n");
    
    return;
    
}


