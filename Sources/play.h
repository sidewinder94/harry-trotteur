#ifndef HTPC_H_DEF_PLAY
#define HTPC_H_DEF_PLAY

/*
 * Fonction gérant les intéractions entre les joueurs et le terrain
 *
 * action: action réalisée par le joueur
 * level: niveau dans lequel réaliser l'action
*/
void
HTPC_play(HTPC_action *action,
          HTPC_level *level);

/*
 * Fonction gérant le déplacement des joueurs.
 * Vérifie la collision
 *
 * action : action à réaliser
 * level : niveau dans lequel réaliser le déplacement
 * player : joueur qui réalise l'action
*/
void
HTPC_play_move(HTPC_action *action,
               HTPC_level *level);

/*
 * Ajout d'un block dans un niveau
 *
 * level: le niveau à modifier
 * pos: position du bloc à modifier
 * block: type de block à ajouter
*/
HTPC_BOOL
HTPC_play_add_block(HTPC_level *level,
                    HTPC_position pos,
                    HTPC_BLOCK block);

HTPC_BOOL
HTPC_play_delete_block(HTPC_level *level,
                       HTPC_position pos,
                       HTPC_BLOCK block);

/*
 * Vérifie la présence de murs et d'objets
 * Récupere les objets pendant la vérification et les utilise si besoin
 *
 * dest : destination demandée par le joueur
 * player : joueur qui réalise l'action
 * level : niveau courant
*/
HTPC_BOOL
HTPC_play_collision(HTPC_position dest,
                    HTPC_level *level,
                    HTPC_BOOL *potion);

/*
 * Permet de prendre et de déposer la cape
 * La cape est déposée sur la case ou se situe le joueur
 *
 * action : action liée à la cape mettre/déposer
 * player : joueur qui réalise l'action
 * level : niveau courant
*/
void
HTPC_play_cloak(HTPC_action *action,
                HTPC_level *level);

#endif

