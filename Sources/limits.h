#ifndef HTPC_H_DEF_LIMITS
#define HTPC_H_DEF_LIMITS

#define NAME_SIZE 255
#define LINE_SIZE 1000
#define COMMAND_SIZE 255
#define MOVE_MAX 50

#endif
