/*                                         
 *     _|    _|  _|_|_|_|_|  _|_|_|      _|_|_|  
 *     _|    _|      _|      _|    _|  _|        
 *     _|_|_|_|      _|      _|_|_|    _|        
 *     _|    _|      _|      _|        _|        
 *     _|    _|      _|      _|          _|_|_|  
 */

#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <string.h>
#ifndef _WIN32
#include <sys/types.h>
#endif

#include "limits.h"
#include "clear.h"
#include "game.h"
#include "init.h"
#include "play.h"
#include "display.h"
#include "input.h"
#include "graphics.h"

const char* menu(const char *dirname);

int main (int argc, char **argv)
{
    char *filename ;
    HTPC_player playerA;
    HTPC_player playerB;
    HTPC_player playerPanda;
    HTPC_level level;
    HTPC_action action;
    HTPC_MODE mode;

    if(argc > 1)
    {
        filename = argv[1]; // récupérer le fichier map en argument
    }
    else
    {
        menu("./map");
    }
    clear();    
    if(!init(&playerA, &playerB, &playerPanda, &level, &mode, filename))
    {
        printf("Problème pendant l'initialisation\n");
        return EXIT_FAILURE;
    }
    
    HTPC_graphics_init(level.size_w, level.size_h, "./tileset32.bmp");

    /* définir le joueur qui commence */
    level.player = &playerPanda;

    /* Afficher le niveau une première fois */
    clear();
    display(&level,&playerA,&playerB);
    HTPC_graphics_display(&level, &playerA, &playerB, &playerPanda);
    
    /* boucle principale */
    while(!level.finish)
    {
        /* changer de joueur */
        if(level.player == &playerA)
            level.player = &playerB;
        else if (level.player == &playerB)
            level.player = &playerPanda;
        else if(level.player == &playerPanda)
            level.player = &playerA;

        /* récupération de la commande */
        input(&action, level.player, level.mode);

        /* lancement d'un tour de jeu */
        HTPC_play(&action, &level);
        
        /* affichage du jeu */
        clear();
        display(&level,&playerA,&playerB);
        HTPC_graphics_display(&level, &playerA, &playerB, &playerPanda);
    }
    
    printf("Le joueur %s gagne ! Felicitations !\n", level.player->name);
    HTPC_graphics_free();
    return EXIT_SUCCESS;
}

const char*
menu(const char *dirname)
{
    DIR* rep = NULL;
	struct dirent* readfile = NULL;
	unsigned int i = 0;
	unsigned int choice = 0;
	char **list = NULL;

	rep = opendir(dirname);
	if (rep == NULL)
	{
	    printf("Need a .htpcmap file as parameter\n");
        printf("Usage : htpc file\n");
	    exit(EXIT_FAILURE);
	}

    /* lire, stocker et afficher */
	while((readfile = readdir(rep)) != NULL)
	{
	   printf("%d. %s\nFaites un choix : \n", i, readfile->d_name);
	   list = realloc(list, sizeof(char*) * i);
	   list[i] = malloc(sizeof(char) * strlen(readfile->d_name));
	   i++;
	}

    /* demander le numéro */
	scanf("%d", choice);
	clean_stdin();

	closedir(rep);
	return list[choice];
}


