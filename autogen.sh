#!/bin/sh

mkdir -p m4
mkdir -p build-aux

aclocal
autoheader
autoconf
automake --add-missing --copy
./configure "$@"
